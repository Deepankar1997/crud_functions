<?php
$servername = "localhost";
$username = "deepankar";
$password = "mindfire";
$dbname = "dks";


class DataEntry
{
	protected $conn;
	public function __construct($servername, $username, $password, $dbname)
	{
		try 
		{
			$dsn = "mysql:host=".$servername.";dbname=".$dbname.";";
			$this->conn = new PDO( $dsn, $username, $password);
			$this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} 
		catch(PDOException $e) 
		{
			echo "Connection failed: " . $e->getMessage();
		}
	}

	//INSERT FUNCTION
	//The parameter $table_value will take the values to be inserted in the table in an associated array form.
	public function insert($table_name, $table_value)
	{
		if ($table_value !== null) {
			$data = array_values($table_value);
		}
		$fields = array_keys($table_value);
		$columns = implode(', ', $fields);
		$rows=array();
		foreach($data as $key){
			$key = '?';
			$rows[] = $key;
		}
		$row_entry = implode(', ', $rows);
		$query = $this->conn->prepare("INSERT INTO $table_name($columns) VALUES ($row_entry);");
		try{
			$query->execute($data);
		}
		catch(PDOException $e){
			echo $e->getMessage();
		}
	}

	//SELECT FUNCTION
	//The parameter $condition will take the column name in which  condition is to be applied(type=string).
	//The parameter $value will take the condition value(type=string). 
	public function select($table_name, $condition, $value)
	{
		$selection_value = array($value);
		$select_stmt = $this->conn->prepare("SELECT * FROM $table_name WHERE $condition = ?");
		try{
			$select_stmt->execute($selection_value);
			$select_stmt->setFetchMode(PDO::FETCH_OBJ);
			$result = $select_stmt->fetch();
			return $result;
		}
		catch(PDOException $e){
			echo $e->getMessage();
		}
	}

	//UPDATE FUNCTION
	//The parameter $new_data will take new values to be updated in form of an associated array.
	//The parameter $id will take the primary key column name(type=string).
	//The parameter $value will take the key value(type=string).
	public function update($table_name, $new_data, $id, $value)
	{
		if($new_data !== null){
			$data = array_values($new_data);
		}
		array_push($data, $value);
		$cols = array_keys($new_data);
		$updated_values = array();
		foreach ($cols as $col)
		{
			$updated_values[] = $col."=?";
		}
		$updated_value = implode(', ',$updated_values);
		$update_stmt = $this->conn->prepare("UPDATE $table_name SET $updated_value WHERE $id=?");
		try{
			$update_stmt->execute($data);
			print_r($update_stmt);
		}
		catch(PDOException $e){
			echo $e->getMessage();
		}
	}

	//DELETE FUNCTION
	//The parameter $condition will take the column name in which  condition is to be applied(type=string).
	//The parameter $value will take the condition value(type=string). 
	public function delete($table_name, $condition, $value)
	{
		$data = array($value);
		$del_stmt = $this->conn->prepare("DELETE FROM $table_name WHERE $condition = ?");
		try{
			$del_stmt->execute($data);
		}
		catch(PDOException $e){
			echo $e->getMessage();
		}
	}
}
?>